#Deploy Spring Boot JAR(s) to Docker container
<p> <b>For development</b> </p>
    - part of Docker Container?
- deploy spring boot to basic Ubuntu/Docker Container

    o **Use** Docker commands (Terminal) to manually deploy to Linux
    o deploy through SSH/bash script?
    o other (Ansible/Chef/Python?)
    
    
#Multiple Docker Containers
- several Spring Boot services per container
- local development - one per container
- AWS different?
Consider deploying to basic Ubuntu Container and then deployment script
can decide which Docker container to deploy boot service - will probably mirror
AWS approach where decision can be either single or multiple containers

##Modify project to deploy Boot Service through Python scripts
- docker run --name -as daemon <container name> Ubuntu
- *ssh into* container
- copy Spring Boot jar to known location
- run Spring Boot jar

---
Sign up for Plaid API key
- Create simple Boot Seervice to access Plaid API - present API as Rest Endpoint
w/JSON contents - can be consumed by front end

- BitBucket as repository or Atlassian Cloud or GitLab Hub
Change Mgmt tool (Jira equivalent)
AWS works with Bitbucket from IDE

Agile development
- User Stories and Epics (or Sagas)

##Approach
1. Create Docker container with Java and directory structure for deploying services
2. Build service
3. Deploy to docker container - docker copy service-deployment-file to docker
4. run
5. test using curl from desktop

Giving World
- API for associating financial accounts to GW account
- pull data for associated accounts
- run sample transactions
- pull new reports on accounts
- verify transactions are recorded

**Docker commands** 

docker run -d --name _<container-name>_ -p <docker port>:<host port> image

docker exec -it <container-name> command

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact