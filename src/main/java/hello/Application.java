package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
public class Application {

    @RequestMapping("/")
    public String home() {
        return "Hello From My Own Spring MVC Dockerized App....";
    }

    @RequestMapping("/info")
    public String getInfo() {
        return "<h1>WTF NO Info?</h1>";
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}